Storage box voor bulk MTG kaarten.

Gedesigned voor mdf platen van 3mm x 600mm x 300mm met een laser van 100µm precisie.

De hinges hangen samen met tandenstokers met een diameter van 2mm.

Het project valt te fabriceren met 5 platen van 60cm op 30cm.

De SVG is verdeeld in 5 Platen waarvan de volgorde niet uitmaakt. Sommige Platen zijn onderverdeeld in Parts, die dienen wel in volgorde gesneden te worden.

Indien de gravure of sommige onderdelen ervan meer moeten uitkomen, kunnen die of de overlay ervan herhaald worden zonder de snijprocedure.
