Laser cut files voor MTG cardstorage en deckboxes.

# Fablab
De files zijn ontworpen voor de laser cut machines van Fablab Leuven, deze machines zijn vrij te gebruiken voor studenten en werknemers van KULeuven, Imec en Flanders Make. Indien andere machines en materialen worden gebruikt, moeten de bestanden daarop aangepast worden.

De files zijn ontworpen voor mdf platen van 300mm x 600 mm x 3mm. (€1.50 in fablab)

Zet de files op een USB stick en vraag de werknemers van Fablab hoe je de machines moet bedienen.

De auteur is niet verantwoordelijk voor ongevallen en schade bij het gebruiken van deze bestanden.

Door coronamaatregels kan het zijn dat assemblage in fablab niet mogelijk is.

# Files bewerken
Om de files te bewerken wordt het open source programma Inkscape aangeraden.

## Afbeeldingen
Het is aangeraden om simpele afbeeldingen in vectorformaat te importeren of om te zetten naar een pad.

Afbeeldingen worden het best in zwart-wit ingeladen. Voorzie genoeg contrast en een goede balans (gebruik de reeds gegeven afbeeldingen als voorbeeld).

## Tekst en font
Tekst in de meegeleverde Magicmedieval font of andere custom fonts wordt best omgezet naar een vectorieel pad.
De computer in fablab zal anders het font niet herkennen en een default font gebruiken.

Tekst in veelvoorkomende fonts kan gewoon in tekstvorm blijven.

# Assemblage
Voor de assemblage is houtlijm nodig. 
Om de houtlijm aan te brengen kan een penseel of wattenstaafjes gebruikt worden.
Verwijder overtollige lijm met keukenpapier.

Het is aangeraden om van onder naar boven en van binnen naar buiten te werken.

Voor de assemblage van de storage box zijn tandenstokers (of iets dergelijk) nodig voor de scharnieren.

## Bewegende delen
Zorg dat bij het lijmen de bewegende delen niet vast komen te zitten.

Bij stroef verloop kan men lichte kracht gebruiken om het hout wat soepeler te laten schuiven.

Als de tollerantie iets te strak is, kan men met schuurpapier of een vijl de hoeken afronden.

## Vernis
De auteur heeft zelf nog geen gebruik gemaakt van vernis, maar dit kan het resultaat mogelijk verbeteren.

## Vragen, opmerkingen en trauma's
Voor vragen en opmerkingen kan men de auteur contacteren via de git service of via vdbroeckb@gmail.com . Voor trauma's wordt best professionele hulp gezocht.


